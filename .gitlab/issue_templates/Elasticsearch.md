---
module-name: "Elasticsearch"
area: "Core Technologies"
maintainers:
  - cleveland
---

## Overview

**Goal**: Set a clear path for Elasticsearch Expert training

**Objectives:**

- Learn about Elasticsearch
- Learn about GitLab's Elasticsearch integration
- Feel comfortable answering some common scenarios.

---

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: Elasticsearch - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical Elasticsearch questions to you

## Stage 1: Become familiar with what Elasticsearch is

1. [ ] Read Elasticsearch Documentation
    - [ ] Read [What is Elasticsearch?](https://www.elastic.co/what-is/elasticsearch)
    - [ ] Read [Elasticsearch Reference – Basic Concepts](https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started-concepts.html)
    - [ ] Read [Elasticsearch Reference – Reading and Writing Documents](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-replication.html)
    - [ ] Read [Elasticsearch Reference – Analysis](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis.html)
1. [ ] Learn about the Elasticsearch Architecture
    - [ ] Watch [a video](https://www.youtube.com/watch?v=YsYUgZu9-Y4) about the basics of the Elasticsearch Architecture
1. [ ] Read the GitLab Documentation
    - [ ] Read [Elasticsearch integration](https://docs.gitlab.com/ee/integration/elasticsearch.html)
    - [ ] Read [Troubleshooting Elasticsearch](https://docs.gitlab.com/ee/administration/troubleshooting/elasticsearch.html)
    - [ ] Read [Elasticsearch lessons learnt for Advanced Global Search](https://about.gitlab.com/blog/2020/04/28/elasticsearch-update/)


## Stage 2: Technical setup

1. [ ] Spin up an Elasticsearch installation. This can be via VM or docker.
1. [ ] Follow [Elasticsearch integration](https://docs.gitlab.com/ee/integration/elasticsearch.html)
      to integrate said Elasticsearch installation with a running GitLab
      installation.
1. [ ] Practice indexing and re-indexing the whole GitLab instance.
1. [ ] Practice re-indexing a specific project.
1. [ ] Feel comfortable determining which projects are not indexed.

## Stage 3: Tickets

1. [ ] Find 10 Solved tickets to get a sense of what gets asked and how others have done troubleshooting in this area.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 5 tickets on this module's topic and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

### Stage 4: Quiz

1. [ ] Contact a current Elasticsearch trainer and let them know you are ready
      for the quiz. They will provide you 3 scenarios in the comment section
      below. You will reply to each scenario as if it was a ticket. You should
      treat these as customer replies and attempt to fully resolve the
      scenario in **one** reply. Example scenarios can be found
      [here](/content/module-elasticsearch/scenarios.md).
1. [ ] Schedule a call with a current Elasticsearch trainer. During this call,
      you will guide them through the following:
    - [ ] Integrating a running Elasticsearch installation with GitLab. This
        does include fully indexing all your data. **Pro-tip**: Do it with
        a smaller data set to speed up the process.
    - [ ] Via the Elasticsearch API, pull the following:
      - [ ] The current health status of the Elasticsearch installation.
      - [ ] Information about the index you just created.
    - [ ] Clear the index status for one project and then re-index that project.
    - [ ] Perform a search for a basic term and verify the results are the same
        via the following methods:
      - [ ] The rails console
      - [ ] The Elasticsearch Search API
      - [ ] The GitLab Search UI
1. [ ] Once you have completed this, have the trainer comment below acknowledging your success.

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

- [ ] Update ...

## Final stage: Completion

1. [ ] Have your manager review this issue.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.
1. [ ] Send a MR to declare yourself an Elasticsearch Expert on the team page
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic. You will now be listed as an expert in Elasticserach on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).

/label ~module
/label ~"Module::Elasticsearch"
