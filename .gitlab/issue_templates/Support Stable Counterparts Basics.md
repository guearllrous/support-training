---
module-name: "Support Stable Counterparts Basics"
area: "Making Support Better"
maintainers:
  - vijirao
---

## Introduction

Welcome to the SSC bootstrapping module - we are glad you want to be a Support Stable Counterpart!

> **As you work through this issue, remember that this issue is [non-confidential and is public](https://about.gitlab.com/handbook/values/#transparency). Please do not add any confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://about.gitlab.com/handbook/support/workflows/working_with_security.html#general-guidelines-1) is OK!**

**Goals of this checklist**

At the end of the checklist, team member should

- Be familiar with the purpose of the Support Stable Counterparts initiative
- Be aware of the resources related to this initiative
- Be familiar with the guidelines and best practices around this initiative

**General Timeline and Expectations** 

- This issue should take you **1 day to complete**.

### Stage 0. Why Stable Counterparts?

1. [ ] Read about the [Stable Counterparts initiative in the leadership page](https://about.gitlab.com/handbook/leadership/#stable-counterparts).
1. [ ] Read more about why and how GitLab has adopted this to our workflows through [An ode to stable counterparts](https://about.gitlab.com/blog/2018/10/16/an-ode-to-stable-counterparts/)
1. [ ] Head over to [Support's Stable Counterparts page](https://about.gitlab.com/handbook/support/support-stable-counterparts.html#expected-outcomes-of-the-support-stable-counterpart-ssc-initiative) to learn about the goals of the initiative in Support, and expectations from the different players.

### Stage 1. Quick Start Checklist

1. [ ] Reach out to your group's PM and EM sync or async.
    - A coffee chat is preferred, however, reach out via Slack if there are timezone and/or availability conflicts.
    - Follow your own style and try to cover the following points with them:
        - Why are you interested in that group
        - What does a regular day look like for the group
        - What are their expectations out of this: level set, manage and align!
1. [ ] Establish a regular cadence for communicating with the Product group.
    - If you are going async, we recommend using a Google doc with a template similar to [this one](https://docs.google.com/document/d/1m9t-sxPzwie2D40cXTwpjnQ2VxYJb3GoEB5ig38Nkmw/edit).
    - Depending on the group and the type of meetings they have, choose the one that would make the most sense for you and Support.
    - Get yourself added to the meeting(s) even if you will be unable to join due to timezone conflicts: you can still read the agenda doc later!
1. [ ] To keep yourself up to date on what's happening with the group,
    - [ ] Join their Slack channel(s).
    - [ ] Subscribe to the pertinent trackers/labels to be aware of new issues and MRs.
        - There are labels for each [section (devops::) and group (group::)](https://about.gitlab.com/handbook/product/categories/#hierarchy). Search for, say, [group::](https://gitlab.com/groups/gitlab-org/-/labels?&search=group%3A%3A) and each label is provided with a link to a view of issues, MRs and epics.
          - Be sure to search at the [group level](https://gitlab.com/groups/gitlab-org/-/issues) to pick up all the projects that make up GitLab.
        - You can [subscribe to labels](https://about.gitlab.com/blog/2016/04/13/feature-highlight-subscribe-to-label/) and set up [email notifications](https://docs.gitlab.com/ee/user/profile/notifications.html).
          - This can create a lot of noise in your inbox and might be hard to properly filter.
          - For issues only (not MRs or Epics) RSS is an option. From the issue search results, you can select the RSS feed button to get a custom RSS feed.
          - Alternatively you can subscribe only to the [Planning Issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Planning%20Issue&first_page_size=20) of your group.
        - Be especially aware of severity::1/severity::2 issues in their areas of the product, including workarounds.
    - [ ] Engineering team pages usually have information about their labels, slack channels, boards, and meetings under 'useful links' at the end.
    - [ ] Join their team sync(s), or, set a reminder to read the notes after the sync(s).
    - [ ] Keep an eye out for interesting tickets affecting the groups you are covering for.
1. [ ] Watch [this internal video](https://drive.google.com/drive/u/0/search?q=parent:1-Cb1GVf5KfsehPDtCWjz6Q1u1b4xE37D) that discusses how to perform the role.
1. [ ] Schedule a pairing or coffee chat with a current SSC to learn more about what they do and share tips of the trade. 
    - If the group you chose already has an SSC, reach out to them to discuss the best way to collaborate and share information.

### Stage 2. Sharing Information

**Sharing Information with Support**

1. [ ] Use the [SWIR](https://docs.google.com/document/d/1eyMzbzImSKNFMpmu33C6imvC1iWEWHREJqaD6mkVDNg/edit#) as your primary mode of communication when sharing group related information with the rest of the Support team.
    - Follow it up with communications in relevant Support Slack channels.
1. [ ] A monthly communication cadence is recommended - however, some groups might not have a lot of updates to share with Support every month. In such instances, make a decision based on your own discretion.
1. [ ] Group related updates and announcements can be:
    - New features added in an upcoming release
    - Bug fixes in an upcoming release
    - Issues likely to generate tickets
    - Major documentation changes
    - Discovered bugs and applicable workarounds
    - Any special processes or troubleshooting workflows that might pertain to the features in your group
1. [ ] Share best practices with new and existing SSCs (see the last stage in this issue for access to the below groups):
    1. [ ] Use the `@support-stable-counterparts` Slack user group to discuss best practices and other tips and tricks.
    1. [ ] Use the `@gitlab-com/support/support-stable-counterparts` GitLab group for process change related ideas, feedback and other discussions.
1. [ ] Create and review relevant training materials and run sessions as needed.
1. [ ] Optionally, consider doing quarterly office hours to chat about your group and share your experiences as an SSC with newer team members.

**Sharing Information with Product Teams**

1. [ ] For sync communication, use the most relevant product team meeting.
    - Be prepared and add your agenda in advance.
1. [ ] For async communication, use the [template](https://docs.google.com/document/d/1m9t-sxPzwie2D40cXTwpjnQ2VxYJb3GoEB5ig38Nkmw/edit) (recommended), or, use Slack.
1. [ ] Loop them in on relevant issues, tickets and Slack threads.
1. [ ] A monthly communication cadence is recommended.
1. [ ] Group related updates and announcements can be:
    - Customer feedback from tickets related to the group
    - Recent trends on tickets related to the group
    - Be the customer's voice and an influencing agent on product related decisions and future roadmap
1. [ ] Optionally, help with questions from non product groups in the Product team's Slack channels if and when possible.

### Stage 3. Ready, set and go!

1. [ ] Create an MR to add your name under 'Support' for the relevant team on [data/stages.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/stages.yml) and add your name to the list on the [source/includes/product/_categories-names.erb](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/sites/handbook/source/includes/product/_categories-names.erb) file, then assign to your manager. 
    1. [ ] Remember, if you are struggling to balance SSC and other work, collaborate with your manager to help with time management and prioritization.
1. [ ] Reach out to your manager (or another manager in #spt_managers Slack channel) to get yourself added to the `@gitlab-com/support/support-stable-counterparts` GitLab group as an `Owner` (https://gitlab.com/groups/gitlab-com/support/support-stable-counterparts/-/group_members)
1. [ ] Create a [single person access request](https://about.gitlab.com/handbook/business-ops/team-member-enablement/onboarding-access-requests/access-requests/#access-requests-ars) to get yourself added to the `@support-stable-counterparts` Slack user group
1. [ ] Announce your new role in the #support-team-chat Slack channel. Here is a sample announcement: "Hi folks! Here is some exciting news - I have signed up as a Support Stable Counterpart for _group_ and I am looking forward to enabling both product and us by acting as a bridge and influencing changes. Reach out to me if you have any questions!".
1. [ ] You're all set to begin your SSC journey, do come back here and the relevant handbook pages to iterate and make changes to improve!
/label ~"Module::Support Stable Counterparts Basics"
