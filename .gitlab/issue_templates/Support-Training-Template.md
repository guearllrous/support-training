## 1. Training name and description
--------------------
Provide a synopsis of the training and what it entails


## 2. Prerequisites
--------------------
Provide a link to or a description of the required prerequisites for completing the training


## 3. Video link
--------------------
Provide a link to the video which should be uploaded to GitLab Unfiltered and added to the play list [Support Training Videos](https://www.youtube.com/watch?v=eR2qn4QHVzI&list=PL05JrBw4t0Kq30AAIrAhnxip2UIPrEiLC)


## 4. Training Steps
--------------------
Add the configuration or training steps in this section


## 5. Troubleshooting Steps
--------------------
Add the steps required for debugging issues
