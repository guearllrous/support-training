---
module-name: "triaging-tickets"
area: "Customer Service"
maintainers:
  - vijirao
---

## Overview

**Goal**: This training module is designed to help team members understand how to triage tickets including the Needs Org and Triage view in Zendesk.

*Length*: This module should take you **4 days** to complete.

**Objectives**: At the end of this module, you should be able to:
- Triage tickets and make sure they are in the right view
- Check ticket metadata and correct them as needed
- Be able to work the Needs Org and Triage view on ZenDesk

**Prerequisites**: Complete the `Zendesk Basics` module prior to starting this module.

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: <module title> - <your name>
1. [ ] Notify your manager to let them know you've started.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

Consider using the Time Tracking functionality so that the estimated length for the module can be refined.

## Stage 1: Let's begin triaging!

**What do we mean by ticket triaging?** Triaging a ticket includes ensuring the ticket has the right form, organization, Support Level and SLA associated with it. While not all tickets will need this, any ticket that ends up in the `Ticket Stage: Needs Org` needs to be triaged!

The triage page is where you would start, and there's a flowchart to help visualize the process: [Triaging Tickets](https://about.gitlab.com/handbook/support/workflows/ticket_triage.html).


While a ticket has [many fields](https://gitlab.com/gitlab-com/support/support-ops/zendesk-ticket-forms-and-fields), initial triaging of any ticket starts with ensuring the 4 fields below are correct to make sure users are taken care of appropriately.

1. [ ] Ticket Form: The form chosen on a ticket will route the ticket to the right set of individuals who can assist the customer on it, and also make sure the right metadata is associated with the ticket. It is very important that all tickets have the right form chosen. Take a look at the current forms in ZenDesk and what each one is for: [Applying the Correct Form](https://about.gitlab.com/handbook/support/workflows/ticket_triage.html#applying-the-correct-form)
1. [ ] Problem Type: Once the correct form is applied, it is important to choose the `Problem Type` of the ticket. Problem types depend on the type of form chosen.
1. [ ] Organization: The organization associated with the user who created the ticket determines many important metadata on the ticket including Subscription, SLA, ARR etc. However, it is common to find tickets coming in without an associated organization. Such tickets end up in the `Ticket Stage: Needs Org`. We'll learn more about this in Stage 2 of this issue.
1. [ ] Priority: It is important to ensure that every ticket has the right Priority associated with it, as it determines the SLAs received on the ticket, and helps us prioritize work.
     1. Read about [Setting Ticket Priority](https://about.gitlab.com/handbook/support/workflows/setting_ticket_priority.html#setting-ticket-priority).
     1. Understand how to [Reset Ticket Priority](https://about.gitlab.com/handbook/support/workflows/setting_ticket_priority.html#resetting-ticket-priority).

**Other Tickets you might see**

1. [ ] If you see a US Federal ticket in the general Support portal views, you can follow the instructions detailed here: [US Federal tickets in Global Support Portal](https://about.gitlab.com/handbook/support/workflows/ticket_triage.html#us-federal-tickets-in-global-support-portal).
1. [ ] You might also see non Support related requests in our views from time to time. Read about how to handle these here: [Other Requests](https://about.gitlab.com/handbook/support/workflows/ticket_triage.html#other-requests).
1. [ ] Occasionally, you might also see Spam tickets in the views. Read about how to handle these here: [Marking tickets as spam in Zendesk](https://about.gitlab.com/handbook/support/workflows/marking_tickets_as_spam.html); more details on the [alternate way here](https://support.zendesk.com/hc/en-us/articles/203691106-Marking-a-ticket-as-spam-and-suspending-the-requester).

## Stage 2: How do I associate a ticket with the right organization?

As mentioned in Stage 1, there are situations in which a ticket can come in without an organization associated to it. Such tickets end up in `Ticket Stage: Needs Org` in Zendesk. This can happen due to several reasons, some of them [listed here](https://about.gitlab.com/handbook/support/workflows/associating_needs_org_tickets_with_orgs.html#use-case).

1. [ ] We have a whole workflow designed for handling such tickets. So how do we know the workflow needs to be applied? If [ZenDesk displays `No Organization` for the ticket](https://about.gitlab.com/handbook/support/workflows/associating_needs_org_tickets_with_orgs.html#determining-if-this-workflow-applies), it means this workflow can be applied!
   1. Follow this workflow for such tickets, and if the workflow doesn't help, you can always reach out to our support ops team via [#support_operations](https://gitlab.slack.com/archives/C018ZGZAMPD) slack channel.
1. [ ] Take a look at the overall flow for org association: [Overall Flow](https://about.gitlab.com/handbook/support/workflows/associating_needs_org_tickets_with_orgs.html#overall-flow)

**Identifying Customers and Users**

The first step when we triage a ticket with no organization is to determine if it is created by a `customer`, `prospect`, `trial` user or `free` user.

1. [ ] Customer: Anyone who has a paid current subscription for SaaS and/or Self Managed GitLab is a customer. All customers receive an SLA on their tickets.
   1. [ ] Read this section on how to identify if the ticket submitter is a customer: [Identifying Customers](https://about.gitlab.com/handbook/support/workflows/associating_needs_org_tickets_with_orgs.html#identifying-customers).
1. [ ] Prospect: Anyone who is trialing GitLab as a proof of concept, with assistance from Sales, is considered a Prospect. Prospects do not receive an SLA on their tickets, unless they are marked as a [Priority Prospect by Sales](https://about.gitlab.com/handbook/support/internal-support/#trials-and-prospect-support).
   1. [ ] Read this section on how to identify if the ticket submitter is a prospect: [Identifying Prospects](https://about.gitlab.com/handbook/support/workflows/associating_needs_org_tickets_with_orgs.html#identifying-and-marking-prospects).
1. [ ] Trial: Anyone who used the free trial form to trial the product is considered a Trial user. Trial users do not get support, as mentioned in the [Free Trial](https://about.gitlab.com/free-trial/) page.
   1. [ ] Read this section on how to identify if the ticket submitter is a trial user: [Identifying and marking Trials](https://about.gitlab.com/handbook/support/workflows/associating_needs_org_tickets_with_orgs.html#identifying-and-marking-trials).
   1. [ ] Note that trial users usually mark themselves as `Ultimate` based on their trial plan, but they are not eligible for support.
1. [ ] Free User: Anyone who uses GitLab but has neither purchased a subscription nor is a Prospect is considered a Free User.
   1. [ ] Read this section on how to identify if the ticket submitter is a free user: [Identifying Free users](https://about.gitlab.com/handbook/support/workflows/associating_needs_org_tickets_with_orgs.html#identifying-free-users).
   1. [ ] If the Free user is requesting Support for anything they are not entitled to, change the Form to `Community Form` and it will activate the [Ticket::Autoresponder::Community tickets](https://gitlab.com/search?utf8=%E2%9C%93&group_id=2573624&project_id=20010334&scope=&search_code=true&snippets=false&repository_ref=master&nav_source=navbar&search=id%3A+360073064519) trigger.
   1. [ ] If the Free user is requesting Support for any items listed in this section, change the Form to `SaaS Account` or appropriate: [Support for Free Plan Users](https://about.gitlab.com/support/statement-of-support.html#free-users)

**Associating an Organization**

Tickets that have been identified to be from `Customers` and `Priority Prospects` have to be associated with their respective organizations.

1. [ ] Finding the organization: While the organization names match most of the time, sometimes the name the customer provides in the support form may or may not match the actual organization name in Zendesk.
   1. [ ] Read this section on finding the appropriate organization name via salesforce and/or Zendesk: [Finding the appropriate organization name](https://about.gitlab.com/handbook/support/workflows/associating_needs_org_tickets_with_orgs.html#finding-the-appropriate-organization-name)
1. [ ] Adding a customer to an organization: Once a user has been identified as a customer, it is important to add them to the organization that has purchased the subscription - this helps ensure that the ticket received the right SLA, and, the next time this user submits a ticket, it would automatically end up with the right SLAs if they are still a customer.
   1. [ ] Read this section on how to add a customer to an organization: [Adding a customer to an organization](https://about.gitlab.com/handbook/support/workflows/associating_needs_org_tickets_with_orgs.html#adding-a-customer-to-an-organization)
      1. [ ] If the user exists in both Salesforce and Zendesk, we can associate the user with the organization as described above.
      1. [ ] If the user exists in Zendesk but not in SFDC, associate the user to the organization in Zendesk, refresh the Zendesk Apps (click the refresh arrow in the top right) and then click the blue button under [SFDC Tool app](https://about.gitlab.com/handbook/support/support-ops/documentation/zendesk_global_apps.html#sfdc-tool).
      1. [ ] If the user does not exist in both Salesforce and Zendesk:
           1. If the user has already been [identified as a customer](https://about.gitlab.com/handbook/support/workflows/associating_needs_org_tickets_with_orgs.html#identifying-customers), associate them with the correct Org in Zendesk as shown above.
           1. Then, message their TAM/AM as described in the [Adding a customer to an organization](https://about.gitlab.com/handbook/support/workflows/associating_needs_org_tickets_with_orgs.html#adding-a-customer-to-an-organization) section. It is recommended that this step is done to ensure correctness of contact information in Salesforce.
1. [ ] Removing a user from an organization: Sometimes, you might end up associating a user with an organization in error.
   1. [ ] Read this section on how to remove a user from an organization: [Removing a user from an organization](https://about.gitlab.com/handbook/support/workflows/associating_needs_org_tickets_with_orgs.html#removing-a-user-from-an-organization)
1. [ ] It is common to see customers filing tickets using their personal email instead of the business email associated with their subscription. Read this section to understand the process for such cases: [Customer is using personal email](https://about.gitlab.com/handbook/support/workflows/associating_needs_org_tickets_with_orgs.html#customer-is-using-personal-email)

## Stage 3: Now that it has the right form and org, I still want to make sure it has the right SLA and is in the correct View!

Once a ticket has the right form, and has an organization associated with it, it is important to make sure that it has the correct SLA and is visible in the correct Zendesk view. So once we associate the ticket with an organization, the following steps should be done.

**SLAs and Support Levels**

SLAs are determined by the [GitLab Support Service Levels](https://about.gitlab.com/support/#gitlab-support-service-levels). Every ticket should have the right SLA associated with it to ensure the right attention is given to it. Once a customer ticket is associated with the correct organization with a valid support level, it should automatically receive the appropriate SLA. There might be cases where this doesn't happen, read on to find out more!

1. [ ] Read about the Service Levels that do not have SLAs: [Appropriate SLA by plan](https://about.gitlab.com/handbook/support/workflows/sla_and_views.html#appropriate-sla-by-plan)
1. [ ] Sometimes, an organization might be incorrectly marked as having `Expired` support level in Salesforce, but will be a currently paying customer. Read this section to understand the process to follow in such cases: [Handling customers with incorrect expired support](https://about.gitlab.com/handbook/support/workflows/sla_and_views.html#handling-customers-with-incorrect-expired-support)
   Note that this process has several actions to be performed simultaneously with the above, to make sure that the customer receives appropriate SLAs and attention on the ticket while we sort out the system issue:
   1. [ ] [Fixing tags for tickets with Expired organization](https://about.gitlab.com/handbook/support/workflows/sla_and_views.html#fixing-tags-for-tickets-with-expired-organization)
   1. [ ] [Verifying that the ticket now has the proper SLA applied](https://about.gitlab.com/handbook/support/workflows/sla_and_views.html#verifying-that-the-ticket-now-has-the-proper-sla-applied)
1. [ ] There may be cases where the organization exists in Zendesk, but its support level does not match the support level in Salesforce. You can follow [the same workflow as above](https://about.gitlab.com/handbook/support/workflows/sla_and_views.html#organization-exists-in-sfdc-but-support-level-does-not-match-zendesk) in such cases as well, or, create an issue in the [support-ops project](https://gitlab.com/gitlab-com/support/support-ops/support-ops-project/-/issues/new).
1. If the ticket is from a customer with a valid support level (not in `Community`, `Expired` or `Hold`), but still doesn't have an SLA:
   1. [ ] Read this section to learn more: [No SLA](https://about.gitlab.com/handbook/support/workflows/sla_and_views.html#no-sla)

**Ticket Views**

Another important aspect of triaging tickets is making sure that they are in the right view. Tickets have to be in appropriate views to be able to receive attention from the right set of folks.

1. [ ] Take a look at the current Zendesk views we use in GitLab Support: [Current Zendesk Views](https://about.gitlab.com/handbook/support/support-ops/documentation/zendesk_global_views.html#current-views)
1. [ ] Organizations that have multiple subscriptions associated with them will result in tickets that receive _all_ the tags associated with that org. For instance, the ticket will have both `silver` and `premium` tags associated with it, resulting in multiple views.
   1. [ ] Read this section to understand how to handle such tickets: [Organizations with multiple subscriptions](https://about.gitlab.com/handbook/support/workflows/sla_and_views.html#organizations-with-multiple-subscriptions)
1. [ ] Similarly, a `Priority Prospect` ticket might show up in both SM and SaaS views. Read this section to understand how to handle such tickets: [Priority prospects showing in multiple views](https://about.gitlab.com/handbook/support/workflows/sla_and_views.html#priority-prospects-showing-in-multiple-views)
1. [ ] Read this section to know the checklist to follow to troubleshoot tickets ending up in the wrong view: [Wrong view](https://about.gitlab.com/handbook/support/workflows/sla_and_views.html#wrong-queue)

## Stage 4: Enough theory, let's do some pairing!

When you reach this stage, post in the [#support_team-chat](https://gitlab.slack.com/archives/CCBJYEWAW) channel to let the team know that you are looking to pair on `Needs Org` tickets, so they can tag you on juicy stuff!

1. Do at least 2 pairing sessions with different support team members focused exclusively on the `Ticket Stage: Needs Org` view. Remember to create [pairing issues](https://gitlab.com/gitlab-com/support/support-pairing/-/tree/master/.gitlab/issue_templates).
   1. [ ] pairing issue: ___
   1. [ ] pairing issue: ___

## Stage 5: It is time to get my hands on them tickets!

Set aside time to focus on the Needs Org view every day for the next two weeks.

1. [ ] Add the link to at least 5 tickets that you triaged from the `Ticket Stage: Needs Org` below:
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Final stage - Tada!

**Summary**

Please review each point below, and check it off.

1. [ ] We receive tickets not just from paying customers, but also from other users. To make sure we are appropriately handling each, triaging a ticket before beginning to technically work on it is very important, especially if it is in `Ticket Stage: Needs Org`.
1. [ ] Start with making sure the correct Ticket Form is being used.
1. [ ] Identify if the ticket is from a customer, prospect, trial user or free user.
1. [ ] Understand the workflow for associating a ticket with an organization.
1. [ ] Make sure that the triaged ticket is receiving the right SLA.
1. [ ] Make sure that the triaged ticket is in the right Zendesk View.
1. [ ] When changing anything related to ticket metadata, make an internal comment on the ticket as it will help the next engineer to understand the context.
1. [ ] You can switch between `Conversations` and `Events` on tickets in Zendesk to see what changed when and by whom.
1. [ ] Feel free to reach out to our friendly Support Ops team through [#support_operations](https://gitlab.slack.com/archives/C018ZGZAMPD) slack channel if you are unsure about something!

**Feedback**

1. [ ] Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).
   1. [ ] Update ...
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.

/label ~module
/label ~"Module::Triaging Tickets"
