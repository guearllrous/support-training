## Prerequisites  

You will need to create the server nodes. For a walk through on how to create the server resources required for this, please watch this [this great video](https://youtu.be/aBF-AyQiFfA) from .

This demonstration references [this existing documentation](https://docs.gitlab.com/ee/administration/postgresql/replication_and_failover.html) and is built as a part of the [3k reference architecture](https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html), as shown here: \


I am using the following server resources:


-  3 GitLab Rails Application Nodes
-  3 PostgreSQL primary node (_also runs Patroni, PG bouncer, and Consul services_)
-  3 PGbouncer nodes _(also runs Consul service_)
-  4 Sidekiq nodes (_also runs Consul service_)
-  3 Redis nodes (_also runs Consul service_)
-  3 Consul nodes (_just consul_)
-  1 Internal load Balancer (_HA Proxy on a vm_)


## Video Walkthrough 

The configuration of migrating from Omnibus to a PostgreSQL Cluster environment is detailed in this [video here](https://gitlab.zoom.us/rec/share/Tqa-KWaHACxlScXgnI0Wz8zKcNAPgOe-BTmoju4cBR6ZmZW5mljkYc4KlQoeVnYt.mQNfZBnA3cjYf7mk?startTime=1657094520000).


## Components in a PostgreSQL Cluster 

Unlike an embedded postgreSQL setup, which hosts and manages the required components all colocated on the same node as rails, PostgreSQL Cluster is spread out over 3 database nodes, 3 PGbouncer nodes, 4 Sidekiq nodes, 3 Consul nodes and a load balancer. While that is 13 nodes, what you get in return is a fault-tolerant, highly available PostgreSQL cluster.


### What Each Service Does 



- [PostgreSQL](https://docs.gitlab.com/omnibus/settings/database.html)

The database for GitLab.

- [Patroni](https://docs.gitlab.com/ee/administration/postgresql/replication_and_failover.html#patroni)  

Patroni is an opinionated solution for PostgreSQL high-availability. It takes the control of PostgreSQL, overrides its configuration, and manages its lifecycle (start, stop, restart). Patroni monitors the cluster and handles any failover. When the primary node fails, it works with Consul to notify PgBouncer. On failure, Patroni handles the transitioning of the old primary to a replica and rejoins it to the cluster automatically.

- [PG Bouncer](https://docs.gitlab.com/ee/administration/postgresql/pgbouncer.html)

This is used to seamlessly migrate database connections between servers in a failover scenario. It can also be used in a non-fault-tolerant setup to pool connections, speeding up response time while reducing resource usage, but we are not using it for that today.

- [Consul](https://docs.gitlab.com/ee/administration/consul.html#configure-the-consul-nodes) 

We use Consul as a notification tool between PGBouncer and other services. You will notice that outside of the Consul server cluster, Consul is also configured to run as an agent on the PostrgreSQL, PGBouncer, Redis and Sidekiq nodes.   

- [Sidekiq](https://docs.gitlab.com/ee/administration/sidekiq.html)

To process background jobs. I don’t know why there is a [specific requirement for 4 nodes](https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html#configure-sidekiq), but that’s what is recommended and what we are using. 

- [Redis](https://docs.gitlab.com/ee/administration/redis/replication_and_failover.html)  

To watch and automatically start the failover procedure.



## Node Configurations


### Redis Nodes


#### Redis `gitlab.rb` Files


##### Redis Node 1 


```
redis-1:~# ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.210
```



```
roles ['redis_sentinel_role', 'redis_master_role']

sentinel['bind'] = '0.0.0.0'
sentinel['quorum'] = 2
sentinel['use_hostnames'] = "False"

redis['bind'] = '0.0.0.0'

redis['port'] = 6379

redis['password'] = 'lX2CSnvfZFQbGo0Z'
redis['master_password'] = 'lX2CSnvfZFQbGo0Z'

redis['master_name'] = 'gitlab-redis'
redis['master_ip'] = '10.142.15.210'

gitlab_rails['auto_migrate'] = false

consul['enable'] = true
consul['configuration'] = {
  bind_addr: '10.142.15.210',
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203)
}
consul['monitoring_service_discovery'] = true

node_exporter['listen_address'] = '0.0.0.0:9100'
redis_exporter['listen_address'] = '0.0.0.0:9121'
redis_exporter['flags'] = {
  'redis.addr' => "redis://localhost:6379",
  'redis.password' => 'lX2CSnvfZFQbGo0Z'
}

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.redis.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



##### Redis Node 2 


```
redis-2:~# ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.207
```



```
roles ['redis_sentinel_role', 'redis_replica_role']

sentinel['bind'] = '0.0.0.0'
sentinel['quorum'] = 2
sentinel['use_hostnames'] = "False"

redis['bind'] = '0.0.0.0'

redis['port'] = 6379

redis['password'] = 'lX2CSnvfZFQbGo0Z'
redis['master_password'] = 'lX2CSnvfZFQbGo0Z'

redis['master_name'] = 'gitlab-redis'
redis['master_ip'] = '10.142.15.210'

gitlab_rails['auto_migrate'] = false

consul['enable'] = true
consul['configuration'] = {
  bind_addr: '10.142.15.207',
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203)
}
consul['monitoring_service_discovery'] = true

node_exporter['listen_address'] = '0.0.0.0:9100'
redis_exporter['listen_address'] = '0.0.0.0:9121'
redis_exporter['flags'] = {
  'redis.addr' => "redis://localhost:6379",
  'redis.password' => 'lX2CSnvfZFQbGo0Z'
}

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.redis.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



##### Redis Node 3 


```
redis-3:~# ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.209
```



```
roles ['redis_sentinel_role', 'redis_replica_role']

sentinel['bind'] = '0.0.0.0'
sentinel['quorum'] = 2
sentinel['use_hostnames'] = "False"

redis['bind'] = '0.0.0.0'

redis['port'] = 6379

redis['password'] = 'lX2CSnvfZFQbGo0Z'
redis['master_password'] = 'lX2CSnvfZFQbGo0Z'

redis['master_name'] = 'gitlab-redis'
redis['master_ip'] = '10.142.15.210'

gitlab_rails['auto_migrate'] = false

consul['enable'] = true
consul['configuration'] = {
  bind_addr: '10.142.15.209',
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203)
}
consul['monitoring_service_discovery'] = true

node_exporter['listen_address'] = '0.0.0.0:9100'
redis_exporter['listen_address'] = '0.0.0.0:9121'
redis_exporter['flags'] = {
  'redis.addr' => "redis://localhost:6379",
  'redis.password' => 'lX2CSnvfZFQbGo0Z'
}

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.redis.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



### Consul Nodes


#### Consul `gitlab.rb` Files 


##### Consul Node 1 


```
consul-1:~# ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.199
```



```
roles ['consul_role']

gitlab_rails['auto_migrate'] = false

consul['configuration'] = {
  server: true,
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203),
  bind_addr: '10.142.15.199',
  log_json: true
}
consul['monitoring_service_discovery'] = true
node_exporter['listen_address'] = '0.0.0.0:9100'

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.consul.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



##### Consul Node 2 


```
consul-2:~# ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.201
```



```
roles ['consul_role']

gitlab_rails['auto_migrate'] = false

consul['configuration'] = {
  server: true,
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203),
  bind_addr: '10.142.15.201',
  log_json: true
}
consul['monitoring_service_discovery'] = true
node_exporter['listen_address'] = '0.0.0.0:9100'

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.consul.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



##### Consul Node 3 


```
consul-3:~# ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.203
```



```
roles ['consul_role']

gitlab_rails['auto_migrate'] = false

consul['configuration'] = {
  server: true,
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203),
  bind_addr: '10.142.15.203',
  log_json: true
}
consul['monitoring_service_discovery'] = true
node_exporter['listen_address'] = '0.0.0.0:9100'

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.consul.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



### Sidekiq Nodes 


#### Sidekiq `gitlab.rb` Files

Note that we define [gitlab_rails['db_load_balancing']](https://docs.gitlab.com/ee/administration/postgresql/database_load_balancing.html#hosts) on these nodes and rails application nodes.


##### Sidekiq Node 1 


```
sidekiq-1:~$ ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.214
```



<table>
  <tr>
  </tr>
</table>



```
# Avoid running unnecessary services on the Sidekiq server
gitaly['enable'] = false
postgresql['enable'] = false
redis['enable'] = false
puma['enable'] = false
gitlab_workhorse['enable'] = false
prometheus['enable'] = false
alertmanager['enable'] = false
grafana['enable'] = false
gitlab_exporter['enable'] = false
nginx['enable'] = false

gitlab_kas['enable'] = false

external_url 'http://refarch-1k-env-700e426c.env-700e426c.gcp.gitlabsandbox.net'

# Sidekiq Config
sidekiq['enable'] = true
sidekiq['listen_address'] = '0.0.0.0'
sidekiq['max_concurrency'] = 10
sidekiq['queue_groups'] = [
  "*",
  "*",
]

# Ensure UIDs and GIDs match between servers for permissions via NFS
user['uid'] = 9000
user['gid'] = 9000
web_server['uid'] = 9001
web_server['gid'] = 9001

# Monitoring
consul['enable'] = true
consul['configuration'] = {
  bind_addr: '10.142.15.214',
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203)
}
consul['monitoring_service_discovery'] = true

node_exporter['listen_address'] = '0.0.0.0:9100'

# Storage Config
## Object Storage - consolidated object storage configuration

gitlab_rails['object_store']['enabled'] = true
gitlab_rails['object_store']['proxy_download'] = true

### Provider-specific connection settings
gitlab_rails['object_store']['connection'] = {
  'provider' => 'Google',
  'google_project' => 'gyoachum-d1847316',
  'google_json_key_location' => '/etc/gitlab/serviceaccount.json'
}

gitlab_rails['object_store']['objects']['artifacts']['bucket'] = "refarch-1k-env-700e426c-artifacts"
gitlab_rails['object_store']['objects']['dependency_proxy']['bucket'] = "refarch-1k-env-700e426c-dependency-proxy"
gitlab_rails['object_store']['objects']['external_diffs']['bucket'] = "refarch-1k-env-700e426c-mr-diffs"
gitlab_rails['object_store']['objects']['lfs']['bucket'] = "refarch-1k-env-700e426c-lfs"
gitlab_rails['object_store']['objects']['packages']['bucket'] = "refarch-1k-env-700e426c-packages"
gitlab_rails['object_store']['objects']['terraform_state']['bucket'] = "refarch-1k-env-700e426c-terraform-state"
gitlab_rails['object_store']['objects']['uploads']['bucket'] = "refarch-1k-env-700e426c-uploads"

## Object Storage - separated backups storage
gitlab_rails['backup_upload_connection'] = {
  'provider' => 'Google',
  'google_project' => 'gyoachum-d1847316',
  'google_json_key_location' => '/etc/gitlab/serviceaccount.json'
}
gitlab_rails['backup_upload_remote_directory'] = "refarch-1k-env-700e426c-backups"

# Components Config
# Redis
## Redis Omnibus - Separate Node(s)
### Combined Queues
#### Separate Multi Node
redis['master_name'] = 'gitlab-redis'
redis['master_password'] = 'lX2CSnvfZFQbGo0Z'

gitlab_rails['redis_sentinels'] = [
  {host: '10.142.15.210', port: 26379},
  {host: '10.142.15.207', port: 26379},
  {host: '10.142.15.209', port: 26379},
]

# Postgres \ Database
gitlab_rails['db_host'] = '10.142.15.208'
gitlab_rails['db_port'] = 6432
gitlab_rails['db_password'] = 'lX2CSnvfZFQbGo0Z'

gitlab_rails['db_load_balancing'] = { 'hosts' => ['10.142.15.224', '10.142.15.223', '10.142.15.222'] }


gitlab_rails['auto_migrate'] = false

gitlab_rails['env'] = {
  # Sidekiq Memory Killer - Disabled by default due to https://gitlab.com/gitlab-org/gitlab/-/issues/225909
  "SIDEKIQ_MEMORY_KILLER_MAX_RSS" => 0

}

# Gitaly
## Gitaly Cluster - Separate Node(s)
gitaly['enable'] = false
git_data_dirs({
  "default" => {
    "gitaly_address" => "tcp://10.142.15.208:2305",
    "gitaly_token" => 'lX2CSnvfZFQbGo0Z'
  }
})

geo_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.geo.*.rb"))
geo_confs.each { |conf|
  from_file conf
}

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.sidekiq.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



##### Sidekiq Node 2 


```
sidekiq-2:~$ ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.213
```



```
# Avoid running unnecessary services on the Sidekiq server
gitaly['enable'] = false
postgresql['enable'] = false
redis['enable'] = false
puma['enable'] = false
gitlab_workhorse['enable'] = false
prometheus['enable'] = false
alertmanager['enable'] = false
grafana['enable'] = false
gitlab_exporter['enable'] = false
nginx['enable'] = false

gitlab_kas['enable'] = false

external_url 'http://refarch-1k-env-700e426c.env-700e426c.gcp.gitlabsandbox.net'

# Sidekiq Config
sidekiq['enable'] = true
sidekiq['listen_address'] = '0.0.0.0'
sidekiq['max_concurrency'] = 10
sidekiq['queue_groups'] = [
  "*",
  "*",
]

# Ensure UIDs and GIDs match between servers for permissions via NFS
user['uid'] = 9000
user['gid'] = 9000
web_server['uid'] = 9001
web_server['gid'] = 9001

# Monitoring
consul['enable'] = true
consul['configuration'] = {
  bind_addr: '10.142.15.213',
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203)
}
consul['monitoring_service_discovery'] = true

node_exporter['listen_address'] = '0.0.0.0:9100'

# Storage Config
## Object Storage - consolidated object storage configuration

gitlab_rails['object_store']['enabled'] = true
gitlab_rails['object_store']['proxy_download'] = true

### Provider-specific connection settings
gitlab_rails['object_store']['connection'] = {
  'provider' => 'Google',
  'google_project' => 'gyoachum-d1847316',
  'google_json_key_location' => '/etc/gitlab/serviceaccount.json'
}

gitlab_rails['object_store']['objects']['artifacts']['bucket'] = "refarch-1k-env-700e426c-artifacts"
gitlab_rails['object_store']['objects']['dependency_proxy']['bucket'] = "refarch-1k-env-700e426c-dependency-proxy"
gitlab_rails['object_store']['objects']['external_diffs']['bucket'] = "refarch-1k-env-700e426c-mr-diffs"
gitlab_rails['object_store']['objects']['lfs']['bucket'] = "refarch-1k-env-700e426c-lfs"
gitlab_rails['object_store']['objects']['packages']['bucket'] = "refarch-1k-env-700e426c-packages"
gitlab_rails['object_store']['objects']['terraform_state']['bucket'] = "refarch-1k-env-700e426c-terraform-state"
gitlab_rails['object_store']['objects']['uploads']['bucket'] = "refarch-1k-env-700e426c-uploads"

## Object Storage - separated backups storage
gitlab_rails['backup_upload_connection'] = {
  'provider' => 'Google',
  'google_project' => 'gyoachum-d1847316',
  'google_json_key_location' => '/etc/gitlab/serviceaccount.json'
}
gitlab_rails['backup_upload_remote_directory'] = "refarch-1k-env-700e426c-backups"

# Components Config
# Redis
## Redis Omnibus - Separate Node(s)
### Combined Queues
#### Separate Multi Node
redis['master_name'] = 'gitlab-redis'
redis['master_password'] = 'lX2CSnvfZFQbGo0Z'

gitlab_rails['redis_sentinels'] = [
  {host: '10.142.15.210', port: 26379},
  {host: '10.142.15.207', port: 26379},
  {host: '10.142.15.209', port: 26379},
]

# Postgres \ Database
gitlab_rails['db_host'] = '10.142.15.208'
gitlab_rails['db_port'] = 6432
gitlab_rails['db_password'] = 'lX2CSnvfZFQbGo0Z'

gitlab_rails['db_load_balancing'] = { 'hosts' => ['10.142.15.224', '10.142.15.223', '10.142.15.222'] }


gitlab_rails['auto_migrate'] = false

gitlab_rails['env'] = {
  # Sidekiq Memory Killer - Disabled by default due to https://gitlab.com/gitlab-org/gitlab/-/issues/225909
  "SIDEKIQ_MEMORY_KILLER_MAX_RSS" => 0

}

# Gitaly
## Gitaly Cluster - Separate Node(s)
gitaly['enable'] = false
git_data_dirs({
  "default" => {
    "gitaly_address" => "tcp://10.142.15.208:2305",
    "gitaly_token" => 'lX2CSnvfZFQbGo0Z'
  }
})

geo_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.geo.*.rb"))
geo_confs.each { |conf|
  from_file conf
}

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.sidekiq.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



##### Sidekiq Node 3


```
sidekiq-3:~$ ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.211
```



```
# Avoid running unnecessary services on the Sidekiq server
gitaly['enable'] = false
postgresql['enable'] = false
redis['enable'] = false
puma['enable'] = false
gitlab_workhorse['enable'] = false
prometheus['enable'] = false
alertmanager['enable'] = false
grafana['enable'] = false
gitlab_exporter['enable'] = false
nginx['enable'] = false

gitlab_kas['enable'] = false

external_url 'http://refarch-1k-env-700e426c.env-700e426c.gcp.gitlabsandbox.net'

# Sidekiq Config
sidekiq['enable'] = true
sidekiq['listen_address'] = '0.0.0.0'
sidekiq['max_concurrency'] = 10
sidekiq['queue_groups'] = [
  "*",
  "*",
]

# Ensure UIDs and GIDs match between servers for permissions via NFS
user['uid'] = 9000
user['gid'] = 9000
web_server['uid'] = 9001
web_server['gid'] = 9001

# Monitoring
consul['enable'] = true
consul['configuration'] = {
  bind_addr: '10.142.15.211',
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203)
}
consul['monitoring_service_discovery'] = true

node_exporter['listen_address'] = '0.0.0.0:9100'

# Storage Config
## Object Storage - consolidated object storage configuration

gitlab_rails['object_store']['enabled'] = true
gitlab_rails['object_store']['proxy_download'] = true

### Provider-specific connection settings
gitlab_rails['object_store']['connection'] = {
  'provider' => 'Google',
  'google_project' => 'gyoachum-d1847316',
  'google_json_key_location' => '/etc/gitlab/serviceaccount.json'
}

gitlab_rails['object_store']['objects']['artifacts']['bucket'] = "refarch-1k-env-700e426c-artifacts"
gitlab_rails['object_store']['objects']['dependency_proxy']['bucket'] = "refarch-1k-env-700e426c-dependency-proxy"
gitlab_rails['object_store']['objects']['external_diffs']['bucket'] = "refarch-1k-env-700e426c-mr-diffs"
gitlab_rails['object_store']['objects']['lfs']['bucket'] = "refarch-1k-env-700e426c-lfs"
gitlab_rails['object_store']['objects']['packages']['bucket'] = "refarch-1k-env-700e426c-packages"
gitlab_rails['object_store']['objects']['terraform_state']['bucket'] = "refarch-1k-env-700e426c-terraform-state"
gitlab_rails['object_store']['objects']['uploads']['bucket'] = "refarch-1k-env-700e426c-uploads"

## Object Storage - separated backups storage
gitlab_rails['backup_upload_connection'] = {
  'provider' => 'Google',
  'google_project' => 'gyoachum-d1847316',
  'google_json_key_location' => '/etc/gitlab/serviceaccount.json'
}
gitlab_rails['backup_upload_remote_directory'] = "refarch-1k-env-700e426c-backups"

# Components Config
# Redis
## Redis Omnibus - Separate Node(s)
### Combined Queues
#### Separate Multi Node
redis['master_name'] = 'gitlab-redis'
redis['master_password'] = 'lX2CSnvfZFQbGo0Z'

gitlab_rails['redis_sentinels'] = [
  {host: '10.142.15.210', port: 26379},
  {host: '10.142.15.207', port: 26379},
  {host: '10.142.15.209', port: 26379},
]

# Postgres \ Database
gitlab_rails['db_host'] = '10.142.15.208'
gitlab_rails['db_port'] = 6432
gitlab_rails['db_password'] = 'lX2CSnvfZFQbGo0Z'

gitlab_rails['db_load_balancing'] = { 'hosts' => ['10.142.15.224', '10.142.15.223', '10.142.15.222'] }


gitlab_rails['auto_migrate'] = false

gitlab_rails['env'] = {
  # Sidekiq Memory Killer - Disabled by default due to https://gitlab.com/gitlab-org/gitlab/-/issues/225909
  "SIDEKIQ_MEMORY_KILLER_MAX_RSS" => 0

}

# Gitaly
## Gitaly Cluster - Separate Node(s)
gitaly['enable'] = false
git_data_dirs({
  "default" => {
    "gitaly_address" => "tcp://10.142.15.208:2305",
    "gitaly_token" => 'lX2CSnvfZFQbGo0Z'
  }
})

geo_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.geo.*.rb"))
geo_confs.each { |conf|
  from_file conf
}

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.sidekiq.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



##### Sidekiq Node 4


```
sidekiq-4:~$ ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.216
```



```
# Avoid running unnecessary services on the Sidekiq server
gitaly['enable'] = false
postgresql['enable'] = false
redis['enable'] = false
puma['enable'] = false
gitlab_workhorse['enable'] = false
prometheus['enable'] = false
alertmanager['enable'] = false
grafana['enable'] = false
gitlab_exporter['enable'] = false
nginx['enable'] = false

gitlab_kas['enable'] = false

external_url 'http://refarch-1k-env-700e426c.env-700e426c.gcp.gitlabsandbox.net'

# Sidekiq Config
sidekiq['enable'] = true
sidekiq['listen_address'] = '0.0.0.0'
sidekiq['max_concurrency'] = 10
sidekiq['queue_groups'] = [
  "*",
  "*",
]

# Ensure UIDs and GIDs match between servers for permissions via NFS
user['uid'] = 9000
user['gid'] = 9000
web_server['uid'] = 9001
web_server['gid'] = 9001

# Monitoring
consul['enable'] = true
consul['configuration'] = {
  bind_addr: '10.142.15.216',
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203)
}
consul['monitoring_service_discovery'] = true

node_exporter['listen_address'] = '0.0.0.0:9100'

# Storage Config
## Object Storage - consolidated object storage configuration

gitlab_rails['object_store']['enabled'] = true
gitlab_rails['object_store']['proxy_download'] = true

### Provider-specific connection settings
gitlab_rails['object_store']['connection'] = {
  'provider' => 'Google',
  'google_project' => 'gyoachum-d1847316',
  'google_json_key_location' => '/etc/gitlab/serviceaccount.json'
}

gitlab_rails['object_store']['objects']['artifacts']['bucket'] = "refarch-1k-env-700e426c-artifacts"
gitlab_rails['object_store']['objects']['dependency_proxy']['bucket'] = "refarch-1k-env-700e426c-dependency-proxy"
gitlab_rails['object_store']['objects']['external_diffs']['bucket'] = "refarch-1k-env-700e426c-mr-diffs"
gitlab_rails['object_store']['objects']['lfs']['bucket'] = "refarch-1k-env-700e426c-lfs"
gitlab_rails['object_store']['objects']['packages']['bucket'] = "refarch-1k-env-700e426c-packages"
gitlab_rails['object_store']['objects']['terraform_state']['bucket'] = "refarch-1k-env-700e426c-terraform-state"
gitlab_rails['object_store']['objects']['uploads']['bucket'] = "refarch-1k-env-700e426c-uploads"

## Object Storage - separated backups storage
gitlab_rails['backup_upload_connection'] = {
  'provider' => 'Google',
  'google_project' => 'gyoachum-d1847316',
  'google_json_key_location' => '/etc/gitlab/serviceaccount.json'
}
gitlab_rails['backup_upload_remote_directory'] = "refarch-1k-env-700e426c-backups"

# Components Config
# Redis
## Redis Omnibus - Separate Node(s)
### Combined Queues
#### Separate Multi Node
redis['master_name'] = 'gitlab-redis'
redis['master_password'] = 'lX2CSnvfZFQbGo0Z'

gitlab_rails['redis_sentinels'] = [
  {host: '10.142.15.210', port: 26379},
  {host: '10.142.15.207', port: 26379},
  {host: '10.142.15.209', port: 26379},
]

# Postgres \ Database
gitlab_rails['db_host'] = '10.142.15.208'
gitlab_rails['db_port'] = 6432
gitlab_rails['db_password'] = 'lX2CSnvfZFQbGo0Z'

gitlab_rails['db_load_balancing'] = { 'hosts' => ['10.142.15.224', '10.142.15.223', '10.142.15.222'] }


gitlab_rails['auto_migrate'] = false

gitlab_rails['env'] = {
  # Sidekiq Memory Killer - Disabled by default due to https://gitlab.com/gitlab-org/gitlab/-/issues/225909
  "SIDEKIQ_MEMORY_KILLER_MAX_RSS" => 0

}

# Gitaly
## Gitaly Cluster - Separate Node(s)
gitaly['enable'] = false
git_data_dirs({
  "default" => {
    "gitaly_address" => "tcp://10.142.15.208:2305",
    "gitaly_token" => 'lX2CSnvfZFQbGo0Z'
  }
})

geo_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.geo.*.rb"))
geo_confs.each { |conf|
  from_file conf
}

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.sidekiq.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



### PostgreSQL Nodes

All database nodes use the same configuration. The leader node is not determined in configuration, and there is no additional or different configuration for either leader or replica nodes. The PostgreSQL nodes **_<span style="text-decoration:underline;">also</span>_** run Patroni, PG bouncer, and Consul services. Notice **_again_** that the bind address for consul specified on each node is referencing the local internal network ip address of the node.


#### PostgreSQL  `gitlab.rb` Files

##### # PostgreSQL Node 1 


```
postgres-1:~# ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.224
```



```
# Disable all components except PostgreSQL, Patroni, PgBouncer and Consul
roles ['patroni_role', 'pgbouncer_role']

# Patroni uses one extra slot per node when initiating the replication
patroni['postgresql']['max_replication_slots'] = 6
patroni['postgresql']['max_wal_senders'] = 7

patroni['remove_data_directory_on_rewind_failure'] = false
patroni['remove_data_directory_on_diverged_timelines'] = false

postgresql['shared_preload_libraries'] = 'pg_stat_statements'

# PostgreSQL configuration
postgresql['listen_address'] = '0.0.0.0'
postgresql['max_connections'] = 500

postgresql['pgbouncer_user_password'] = "527139d638cf476ed2be2f29aa1c6597"
postgresql['sql_user_password'] = "8b6d9ca67bdc78153a319d097ce0ee25"

postgresql['trust_auth_cidr_addresses'] = ['0.0.0.0/0']
postgresql['md5_auth_cidr_addresses'] = ['0.0.0.0/0']

gitlab_rails['auto_migrate'] = false

# PgBouncer (for load balancing reads)
pgbouncer['databases'] = {
  gitlabhq_production: {
    host: "127.0.0.1",
    user: "pgbouncer",
    password: "527139d638cf476ed2be2f29aa1c6597"
  }
}
pgbouncer['max_db_connections'] = 150

# Consul
consul['services'] = %w(postgresql)
consul['configuration'] = {
  bind_addr: '10.142.15.224',
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203)
}
consul['monitoring_service_discovery'] = true

# Monitoring
postgres_exporter['listen_address'] = '0.0.0.0:9187'
node_exporter['listen_address'] = '0.0.0.0:9100'

geo_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.geo.*.rb"))
geo_confs.each { |conf|
  from_file conf
}

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.postgres.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



##### # PostgreSQL Node 2


```
postgres-2:~# ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.223
```



```
# Disable all components except PostgreSQL, Patroni, PgBouncer and Consul
roles ['patroni_role', 'pgbouncer_role']

# Patroni uses one extra slot per node when initiating the replication
patroni['postgresql']['max_replication_slots'] = 6
patroni['postgresql']['max_wal_senders'] = 7

patroni['remove_data_directory_on_rewind_failure'] = false
patroni['remove_data_directory_on_diverged_timelines'] = false

postgresql['shared_preload_libraries'] = 'pg_stat_statements'

# PostgreSQL configuration
postgresql['listen_address'] = '0.0.0.0'
postgresql['max_connections'] = 500

postgresql['pgbouncer_user_password'] = "527139d638cf476ed2be2f29aa1c6597"
postgresql['sql_user_password'] = "8b6d9ca67bdc78153a319d097ce0ee25"

postgresql['trust_auth_cidr_addresses'] = ['0.0.0.0/0']
postgresql['md5_auth_cidr_addresses'] = ['0.0.0.0/0']

gitlab_rails['auto_migrate'] = false

# PgBouncer (for load balancing reads)
pgbouncer['databases'] = {
  gitlabhq_production: {
    host: "127.0.0.1",
    user: "pgbouncer",
    password: "527139d638cf476ed2be2f29aa1c6597"
  }
}
pgbouncer['max_db_connections'] = 150

# Consul
consul['services'] = %w(postgresql)
consul['configuration'] = {
  bind_addr: '10.142.15.223',
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203)
}
consul['monitoring_service_discovery'] = true

# Monitoring
postgres_exporter['listen_address'] = '0.0.0.0:9187'
node_exporter['listen_address'] = '0.0.0.0:9100'

geo_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.geo.*.rb"))
geo_confs.each { |conf|
  from_file conf
}

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.postgres.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



##### # PostgreSQL Node 3


```
postgres-3:~# ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.222
```



```
# Disable all components except PostgreSQL, Patroni, PgBouncer and Consul
roles ['patroni_role', 'pgbouncer_role']

# Patroni uses one extra slot per node when initiating the replication
patroni['postgresql']['max_replication_slots'] = 6
patroni['postgresql']['max_wal_senders'] = 7

patroni['remove_data_directory_on_rewind_failure'] = false
patroni['remove_data_directory_on_diverged_timelines'] = false

postgresql['shared_preload_libraries'] = 'pg_stat_statements'

# PostgreSQL configuration
postgresql['listen_address'] = '0.0.0.0'
postgresql['max_connections'] = 500

postgresql['pgbouncer_user_password'] = "527139d638cf476ed2be2f29aa1c6597"
postgresql['sql_user_password'] = "8b6d9ca67bdc78153a319d097ce0ee25"

postgresql['trust_auth_cidr_addresses'] = ['0.0.0.0/0']
postgresql['md5_auth_cidr_addresses'] = ['0.0.0.0/0']

gitlab_rails['auto_migrate'] = false

# PgBouncer (for load balancing reads)
pgbouncer['databases'] = {
  gitlabhq_production: {
    host: "127.0.0.1",
    user: "pgbouncer",
    password: "527139d638cf476ed2be2f29aa1c6597"
  }
}
pgbouncer['max_db_connections'] = 150

# Consul
consul['services'] = %w(postgresql)
consul['configuration'] = {
  bind_addr: '10.142.15.222',
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203)
}
consul['monitoring_service_discovery'] = true

# Monitoring
postgres_exporter['listen_address'] = '0.0.0.0:9187'
node_exporter['listen_address'] = '0.0.0.0:9100'

geo_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.geo.*.rb"))
geo_confs.each { |conf|
  from_file conf
}

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.postgres.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



### PGBouncer Nodes

The following configurations are for the PGBouncer Cluster nodes. Consul runs with Pgbouncer services on the PGBouncer nodes. The specific change per node is the consul bind address for the **local** consul service. **<span style="text-decoration:underline;">This is different</span>** from the consul cluster nodes.


#### PGBouncer `gitlab.rb` Files


##### PGBouncer Node 1


```
pgbouncer-1:~# ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.217
```



```
# Disable all components except Pgbouncer and Consul agent
roles ['pgbouncer_role']

# Configure Pgbouncer

pgbouncer['users'] = {
  'gitlab-consul': {
    password: "d9f19f8f9dae0f6c1e8e562be13fdbff"
  },
  'pgbouncer': {
    password: "527139d638cf476ed2be2f29aa1c6597"
  }
}

pgbouncer['max_db_connections'] = 150

# Configure Consul agent
consul['watchers'] = %w(postgresql)
consul['configuration'] = {
  bind_addr: '10.142.15.217',
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203)
}
consul['monitoring_service_discovery'] = true

node_exporter['listen_address'] = '0.0.0.0:9100'

pgbouncer_exporter['enable'] = true
pgbouncer_exporter['listen_address'] = '0.0.0.0:9188'

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.pgbouncer.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



##### PGBouncer Node 2


```
pgbouncer-2:~# ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.215
```



```
# Disable all components except Pgbouncer and Consul agent
roles ['pgbouncer_role']

# Configure Pgbouncer

pgbouncer['users'] = {
  'gitlab-consul': {
    password: "d9f19f8f9dae0f6c1e8e562be13fdbff"
  },
  'pgbouncer': {
    password: "527139d638cf476ed2be2f29aa1c6597"
  }
}

pgbouncer['max_db_connections'] = 150

# Configure Consul agent
consul['watchers'] = %w(postgresql)
consul['configuration'] = {
  bind_addr: '10.142.15.215',
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203)
}
consul['monitoring_service_discovery'] = true

node_exporter['listen_address'] = '0.0.0.0:9100'

pgbouncer_exporter['enable'] = true
pgbouncer_exporter['listen_address'] = '0.0.0.0:9188'

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.pgbouncer.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



##### PGBouncer Node 3


```
pgbouncer-3:~$ ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.212
```



```
# Disable all components except Pgbouncer and Consul agent
roles ['pgbouncer_role']

# Configure Pgbouncer

pgbouncer['users'] = {
  'gitlab-consul': {
    password: "d9f19f8f9dae0f6c1e8e562be13fdbff"
  },
  'pgbouncer': {
    password: "527139d638cf476ed2be2f29aa1c6597"
  }
}

pgbouncer['max_db_connections'] = 150

# Configure Consul agent
consul['watchers'] = %w(postgresql)
consul['configuration'] = {
  bind_addr: '10.142.15.212',
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203)
}
consul['monitoring_service_discovery'] = true

node_exporter['listen_address'] = '0.0.0.0:9100'

pgbouncer_exporter['enable'] = true
pgbouncer_exporter['listen_address'] = '0.0.0.0:9188'

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.pgbouncer.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



### Internal Load-Balancer Node
We’ll also need to make use of an [internal load balancer](https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html#configure-the-internal-load-balancer), to balance internal connections to [PgBouncer](https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html#configure-pgbouncer).  For this demonstration, I’m using [HAProxy](https://www.haproxy.org/) with the following configuration in `/opt/haproxy/haproxy.cfg`:  \
 \
_Note that we are also using HAProxy for Praefect and GitLab Rails._ 


##### HAProxy Node 1


```
haproxy-internal-1:/# ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.208
```



```
global
    log /dev/log local0
    log localhost local1 notice
    log stdout format raw local0
    maxconn 10000
    daemon

defaults
    log global
    mode http
    default-server inter 10s fall 3 rise 2
    balance leastconn
    compression algo gzip
    compression type text/css text/html text/plain text/xml application/json
    option httplog
    option dontlognull
    retries 3
    timeout connect 30s
    timeout client 305s
    timeout server 305s

frontend internal-gitlab-rails-tcp-in
    bind *:80
    mode tcp
    option tcplog
    option clitcpka

    default_backend gitlab-rails

frontend internal-pgbouncer-tcp-in
    bind *:6432
    mode tcp
    option tcplog
    option clitcpka

    default_backend pgbouncer

frontend internal-praefect-tcp-in
    bind *:2305
    mode tcp
    option tcplog
    option clitcpka

    default_backend praefect

backend gitlab-rails
    mode tcp
    option tcp-check
    option srvtcpka

    server gitlab-rails1 10.142.15.220:80 check inter 3s fall 1
    server gitlab-rails2 10.142.15.221:80 check inter 3s fall 1
    server gitlab-rails3 10.142.15.218:80 check inter 3s fall 1

backend pgbouncer
    mode tcp
    option tcp-check
    option srvtcpka

    server pgbouncer1 10.142.15.217:6432 check inter 3s fall 1
    server pgbouncer2 10.142.15.215:6432 check inter 3s fall 1
    server pgbouncer3 10.142.15.212:6432 check inter 3s fall 1

backend praefect
    mode tcp
    option tcp-check
    option srvtcpka

    server praefect1 10.142.15.194:2305 check inter 3s fall 1
    server praefect2 10.142.15.196:2305 check inter 3s fall 1
    server praefect3 10.142.15.195:2305 check inter 3s fall 1

listen stats
    bind *:1936
    http-request use-service prometheus-exporter if { path /metrics }
    mode http
    stats enable
    stats uri /
    stats refresh 10s
```



### Rails Application Nodes

Note that we define [gitlab_rails['db_load_balancing']](https://docs.gitlab.com/ee/administration/postgresql/database_load_balancing.html#hosts) on these nodes, and the aforementioned Sidekiq nodes. The DB host is the internal IP for the Internal HAProxy node over [port 6432, which is assigned to PGBouncer](https://docs.gitlab.com/ee/administration/package_information/defaults.html), that HAProxy is serving.


```
# Postgres
## Postgres Omnibus - Separate Node(s)
postgresql['enable'] = false

gitlab_rails['db_host'] = "10.142.15.208"
gitlab_rails['db_port'] = 6432
gitlab_rails['db_password'] = 'lX2CSnvfZFQbGo0Z'
gitlab_rails['auto_migrate'] = false

gitlab_rails['db_load_balancing'] = { 'hosts' => ['10.142.15.224', '10.142.15.223', '10.142.15.222'] }
—-
```



#### Rails Application `gitlab.rb` Files 


##### Rails Application Node 1 


```
gitlab-rails-1:~# ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.220
```



```
roles ['application_role']

external_url 'http://refarch-1k-env-700e426c.env-700e426c.gcp.gitlabsandbox.net'
registry['enable'] = false

unicorn['enable'] = false
puma['enable'] = true

puma['listen'] = '0.0.0.0'

gitlab_rails['initial_root_password'] = "lX2CSnvfZFQbGo0Z"
gitlab_rails['gitlab_shell_ssh_port'] = "2222"

# Ensure UIDs and GIDs match between servers for permissions via NFS
user['uid'] = 9000
user['gid'] = 9000
web_server['uid'] = 9001
web_server['gid'] = 9001

# Nginx Config
nginx['enable'] = true
nginx['status']['options'] = {
  "server_tokens" => "off",
  "access_log" => "off",
  "deny" => "all",
  "allow" => "10.142.15.206"
}

### External merge request diffs - store outdated diffs in object storage
gitlab_rails['external_diffs_when'] = 'outdated'

# Storage Config
## Object Storage - consolidated object storage configuration

gitlab_rails['object_store']['enabled'] = true
gitlab_rails['object_store']['proxy_download'] = true

### Provider-specific connection settings
gitlab_rails['object_store']['connection'] = {
  'provider' => 'Google',
  'google_project' => 'gyoachum-d1847316',
  'google_json_key_location' => '/etc/gitlab/serviceaccount.json'
}

gitlab_rails['object_store']['objects']['artifacts']['bucket'] = "refarch-1k-env-700e426c-artifacts"
gitlab_rails['object_store']['objects']['dependency_proxy']['bucket'] = "refarch-1k-env-700e426c-dependency-proxy"
gitlab_rails['object_store']['objects']['external_diffs']['bucket'] = "refarch-1k-env-700e426c-mr-diffs"
gitlab_rails['object_store']['objects']['lfs']['bucket'] = "refarch-1k-env-700e426c-lfs"
gitlab_rails['object_store']['objects']['packages']['bucket'] = "refarch-1k-env-700e426c-packages"
gitlab_rails['object_store']['objects']['terraform_state']['bucket'] = "refarch-1k-env-700e426c-terraform-state"
gitlab_rails['object_store']['objects']['uploads']['bucket'] = "refarch-1k-env-700e426c-uploads"

## Object Storage - separated backups storage
gitlab_rails['backup_upload_connection'] = {
  'provider' => 'Google',
  'google_project' => 'gyoachum-d1847316',
  'google_json_key_location' => '/etc/gitlab/serviceaccount.json'
}
gitlab_rails['backup_upload_remote_directory'] = "refarch-1k-env-700e426c-backups"

# Monitoring
consul['enable'] = true
consul['configuration'] = {
  bind_addr: '10.142.15.220',
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203),
}
consul['monitoring_service_discovery'] = true

node_exporter['listen_address'] = '0.0.0.0:9100'
gitlab_rails['monitoring_whitelist'] = ['0.0.0.0/0']
gitlab_workhorse['prometheus_listen_addr'] = '0.0.0.0:9229'

# Components Config
# Redis
## Redis Omnibus - Separate Node(s)
### Combined Queues
#### Separate Multi Node
redis['master_name'] = 'gitlab-redis'
redis['master_password'] = 'lX2CSnvfZFQbGo0Z'

gitlab_rails['redis_sentinels'] = [
  {host: '10.142.15.210', port: 26379},
  {host: '10.142.15.207', port: 26379},
  {host: '10.142.15.209', port: 26379},
]

## Sidekiq - Separate Node(s)
sidekiq['enable'] = false

# Postgres
## Postgres Omnibus - Separate Node(s)
postgresql['enable'] = false

gitlab_rails['db_host'] = "10.142.15.208"
gitlab_rails['db_port'] = 6432
gitlab_rails['db_password'] = 'lX2CSnvfZFQbGo0Z'
gitlab_rails['auto_migrate'] = false

gitlab_rails['db_load_balancing'] = { 'hosts' => ['10.142.15.224', '10.142.15.223', '10.142.15.222'] }

## Monitor - Separate Node(s)
prometheus['enable'] = false
grafana['enable'] = false

gitlab_rails['env'] = {}

gitlab_workhorse['env'] = {}

## Gitaly Cluster - Separate Node(s)
gitaly['enable'] = false
git_data_dirs({
  "default" => {
    "gitaly_address" => "tcp://10.142.15.208:2305",
    "gitaly_token" => 'lX2CSnvfZFQbGo0Z'
  }
})

geo_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.geo.*.rb"))
geo_confs.each { |conf|
  from_file conf
}

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.gitlab_rails.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



##### Rails Application Node 2


```
gitlab-rails-2:~# ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.221
```



```
roles ['application_role']

external_url 'http://refarch-1k-env-700e426c.env-700e426c.gcp.gitlabsandbox.net'
registry['enable'] = false

unicorn['enable'] = false
puma['enable'] = true

puma['listen'] = '0.0.0.0'

gitlab_rails['initial_root_password'] = "lX2CSnvfZFQbGo0Z"
gitlab_rails['gitlab_shell_ssh_port'] = "2222"

# Ensure UIDs and GIDs match between servers for permissions via NFS
user['uid'] = 9000
user['gid'] = 9000
web_server['uid'] = 9001
web_server['gid'] = 9001

# Nginx Config
nginx['enable'] = true
nginx['status']['options'] = {
  "server_tokens" => "off",
  "access_log" => "off",
  "deny" => "all",
  "allow" => "10.142.15.206"
}

### External merge request diffs - store outdated diffs in object storage
gitlab_rails['external_diffs_when'] = 'outdated'

# Storage Config
## Object Storage - consolidated object storage configuration

gitlab_rails['object_store']['enabled'] = true
gitlab_rails['object_store']['proxy_download'] = true

### Provider-specific connection settings
gitlab_rails['object_store']['connection'] = {
  'provider' => 'Google',
  'google_project' => 'gyoachum-d1847316',
  'google_json_key_location' => '/etc/gitlab/serviceaccount.json'
}

gitlab_rails['object_store']['objects']['artifacts']['bucket'] = "refarch-1k-env-700e426c-artifacts"
gitlab_rails['object_store']['objects']['dependency_proxy']['bucket'] = "refarch-1k-env-700e426c-dependency-proxy"
gitlab_rails['object_store']['objects']['external_diffs']['bucket'] = "refarch-1k-env-700e426c-mr-diffs"
gitlab_rails['object_store']['objects']['lfs']['bucket'] = "refarch-1k-env-700e426c-lfs"
gitlab_rails['object_store']['objects']['packages']['bucket'] = "refarch-1k-env-700e426c-packages"
gitlab_rails['object_store']['objects']['terraform_state']['bucket'] = "refarch-1k-env-700e426c-terraform-state"
gitlab_rails['object_store']['objects']['uploads']['bucket'] = "refarch-1k-env-700e426c-uploads"

## Object Storage - separated backups storage
gitlab_rails['backup_upload_connection'] = {
  'provider' => 'Google',
  'google_project' => 'gyoachum-d1847316',
  'google_json_key_location' => '/etc/gitlab/serviceaccount.json'
}
gitlab_rails['backup_upload_remote_directory'] = "refarch-1k-env-700e426c-backups"

# Monitoring
consul['enable'] = true
consul['configuration'] = {
  bind_addr: '10.142.15.221',
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203),
}
consul['monitoring_service_discovery'] = true

node_exporter['listen_address'] = '0.0.0.0:9100'
gitlab_rails['monitoring_whitelist'] = ['0.0.0.0/0']
gitlab_workhorse['prometheus_listen_addr'] = '0.0.0.0:9229'

# Components Config
# Redis
## Redis Omnibus - Separate Node(s)
### Combined Queues
#### Separate Multi Node
redis['master_name'] = 'gitlab-redis'
redis['master_password'] = 'lX2CSnvfZFQbGo0Z'

gitlab_rails['redis_sentinels'] = [
  {host: '10.142.15.210', port: 26379},
  {host: '10.142.15.207', port: 26379},
  {host: '10.142.15.209', port: 26379},
]

## Sidekiq - Separate Node(s)
sidekiq['enable'] = false

# Postgres
## Postgres Omnibus - Separate Node(s)
postgresql['enable'] = false

gitlab_rails['db_host'] = "10.142.15.208"
gitlab_rails['db_port'] = 6432
gitlab_rails['db_password'] = 'lX2CSnvfZFQbGo0Z'
gitlab_rails['auto_migrate'] = false

gitlab_rails['db_load_balancing'] = { 'hosts' => ['10.142.15.224', '10.142.15.223', '10.142.15.222'] }

## Monitor - Separate Node(s)
prometheus['enable'] = false
grafana['enable'] = false

gitlab_rails['env'] = {}

gitlab_workhorse['env'] = {}

## Gitaly Cluster - Separate Node(s)
gitaly['enable'] = false
git_data_dirs({
  "default" => {
    "gitaly_address" => "tcp://10.142.15.208:2305",
    "gitaly_token" => 'lX2CSnvfZFQbGo0Z'
  }
})

geo_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.geo.*.rb"))
geo_confs.each { |conf|
  from_file conf
}

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.gitlab_rails.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



##### Rails Application Node 3


```
gitlab-rails-3:~# ifconfig | grep "inet 10." | cut -c 1-26
        inet 10.142.15.218
```



```
roles ['application_role']

external_url 'http://refarch-1k-env-700e426c.env-700e426c.gcp.gitlabsandbox.net'
registry['enable'] = false

unicorn['enable'] = false
puma['enable'] = true

puma['listen'] = '0.0.0.0'

gitlab_rails['initial_root_password'] = "lX2CSnvfZFQbGo0Z"
gitlab_rails['gitlab_shell_ssh_port'] = "2222"

# Ensure UIDs and GIDs match between servers for permissions via NFS
user['uid'] = 9000
user['gid'] = 9000
web_server['uid'] = 9001
web_server['gid'] = 9001

# Nginx Config
nginx['enable'] = true
nginx['status']['options'] = {
  "server_tokens" => "off",
  "access_log" => "off",
  "deny" => "all",
  "allow" => "10.142.15.206"
}

### External merge request diffs - store outdated diffs in object storage
gitlab_rails['external_diffs_when'] = 'outdated'

# Storage Config
## Object Storage - consolidated object storage configuration

gitlab_rails['object_store']['enabled'] = true
gitlab_rails['object_store']['proxy_download'] = true

### Provider-specific connection settings
gitlab_rails['object_store']['connection'] = {
  'provider' => 'Google',
  'google_project' => 'gyoachum-d1847316',
  'google_json_key_location' => '/etc/gitlab/serviceaccount.json'
}

gitlab_rails['object_store']['objects']['artifacts']['bucket'] = "refarch-1k-env-700e426c-artifacts"
gitlab_rails['object_store']['objects']['dependency_proxy']['bucket'] = "refarch-1k-env-700e426c-dependency-proxy"
gitlab_rails['object_store']['objects']['external_diffs']['bucket'] = "refarch-1k-env-700e426c-mr-diffs"
gitlab_rails['object_store']['objects']['lfs']['bucket'] = "refarch-1k-env-700e426c-lfs"
gitlab_rails['object_store']['objects']['packages']['bucket'] = "refarch-1k-env-700e426c-packages"
gitlab_rails['object_store']['objects']['terraform_state']['bucket'] = "refarch-1k-env-700e426c-terraform-state"
gitlab_rails['object_store']['objects']['uploads']['bucket'] = "refarch-1k-env-700e426c-uploads"

## Object Storage - separated backups storage
gitlab_rails['backup_upload_connection'] = {
  'provider' => 'Google',
  'google_project' => 'gyoachum-d1847316',
  'google_json_key_location' => '/etc/gitlab/serviceaccount.json'
}
gitlab_rails['backup_upload_remote_directory'] = "refarch-1k-env-700e426c-backups"

# Monitoring
consul['enable'] = true
consul['configuration'] = {
  bind_addr: '10.142.15.218',
  retry_join: %w(10.142.15.199 10.142.15.201 10.142.15.203),
}
consul['monitoring_service_discovery'] = true

node_exporter['listen_address'] = '0.0.0.0:9100'
gitlab_rails['monitoring_whitelist'] = ['0.0.0.0/0']
gitlab_workhorse['prometheus_listen_addr'] = '0.0.0.0:9229'

# Components Config
# Redis
## Redis Omnibus - Separate Node(s)
### Combined Queues
#### Separate Multi Node
redis['master_name'] = 'gitlab-redis'
redis['master_password'] = 'lX2CSnvfZFQbGo0Z'

gitlab_rails['redis_sentinels'] = [
  {host: '10.142.15.210', port: 26379},
  {host: '10.142.15.207', port: 26379},
  {host: '10.142.15.209', port: 26379},
]

## Sidekiq - Separate Node(s)
sidekiq['enable'] = false

# Postgres
## Postgres Omnibus - Separate Node(s)
postgresql['enable'] = false

gitlab_rails['db_host'] = "10.142.15.208"
gitlab_rails['db_port'] = 6432
gitlab_rails['db_password'] = 'lX2CSnvfZFQbGo0Z'
gitlab_rails['auto_migrate'] = false

gitlab_rails['db_load_balancing'] = { 'hosts' => ['10.142.15.224', '10.142.15.223', '10.142.15.222'] }

## Monitor - Separate Node(s)
prometheus['enable'] = false
grafana['enable'] = false

gitlab_rails['env'] = {}

gitlab_workhorse['env'] = {}

## Gitaly Cluster - Separate Node(s)
gitaly['enable'] = false
git_data_dirs({
  "default" => {
    "gitaly_address" => "tcp://10.142.15.208:2305",
    "gitaly_token" => 'lX2CSnvfZFQbGo0Z'
  }
})

geo_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.geo.*.rb"))
geo_confs.each { |conf|
  from_file conf
}

custom_confs = Dir.glob(File.join("/etc/gitlab/", "gitlab.gitlab_rails.*.rb"))
custom_confs.each { |conf|
  from_file conf
}
```



## Validation Methods


#### Patroni \
 {#patroni}


```
postgres-1:~# gitlab-ctl patroni members
+ Cluster: postgresql-ha (7116509799009299863) -------------------+---------------+---------+---------+----+-----------+
| Member                                                          | Host          | Role    | State   | TL | Lag in MB |
+-----------------------------------------------------------------+---------------+---------+---------+----+-----------+
| refarch-1k-env-700e426c-postgres-1.c.gyoachum-d1847316.internal | 10.142.15.224 | Leader  | running |  2 |           |
| refarch-1k-env-700e426c-postgres-2.c.gyoachum-d1847316.internal | 10.142.15.223 | Replica | running |  2 |         0 |
| refarch-1k-env-700e426c-postgres-3.c.gyoachum-d1847316.internal | 10.142.15.222 | Replica | running |  2 |         0 |
+-----------------------------------------------------------------+---------------+---------+---------+----+-----------+
```



```
postgres-2:~# sudo gitlab-ctl tail patroni
==> /var/log/gitlab/patroni/state <==

==> /var/log/gitlab/patroni/current <==
2022-07-06_07:40:06.67524 2022-07-06 07:40:06,674 INFO: no action. I am a secondary (refarch-1k-env-700e426c-postgres-2.c.gyoachum-d1847316.internal) and following a leader (refarch-1k-env-700e426c-postgres-1.c.gyoachum-d1847316.internal)
2022-07-06_07:40:16.72260 2022-07-06 07:40:16,721 INFO: no action. I am a secondary (refarch-1k-env-700e426c-postgres-2.c.gyoachum-d1847316.internal) and following a leader (refarch-1k-env-700e426c-postgres-1.c.gyoachum-d1847316.internal)
2022-07-06_07:40:26.73350 2022-07-06 07:40:26,732 INFO: no action. I am a secondary (refarch-1k-env-700e426c-postgres-2.c.gyoachum-d1847316.internal) and following a leader (refarch-1k-env-700e426c-postgres-1.c.gyoachum-d1847316.internal)
2022-07-06_07:40:36.53298 2022-07-06 07:40:36,532 INFO: no action. I am a secondary (refarch-1k-env-700e426c-postgres-2.c.gyoachum-d1847316.internal) and following a leader (refarch-1k-env-700e426c-postgres-1.c.gyoachum-d1847316.internal)
2022-07-06_07:40:46.51594 2022-07-06 07:40:46,515 INFO: no action. I am a secondary (refarch-1k-env-700e426c-postgres-2.c.gyoachum-d1847316.internal) and following a leader (refarch-1k-env-700e426c-postgres-1.c.gyoachum-d1847316.internal)
2022-07-06_07:40:56.56765 2022-07-06 07:40:56,566 INFO: no action. I am a secondary (refarch-1k-env-700e426c-postgres-2.c.gyoachum-d1847316.internal) and following a leader (refarch-1k-env-700e426c-postgres-1.c.gyoachum-d1847316.internal)
2022-07-06_07:41:06.64575 2022-07-06 07:41:06,644 INFO: no action. I am a secondary (refarch-1k-env-700e426c-postgres-2.c.gyoachum-d1847316.internal) and following a leader (refarch-1k-env-700e426c-postgres-1.c.gyoachum-d1847316.internal)
2022-07-06_07:41:16.36259 2022-07-06 07:41:16,361 INFO: no action. I am a secondary (refarch-1k-env-700e426c-postgres-2.c.gyoachum-d1847316.internal) and following a leader (refarch-1k-env-700e426c-postgres-1.c.gyoachum-d1847316.internal)
2022-07-06_07:41:26.82712 2022-07-06 07:41:26,826 INFO: no action. I am a secondary (refarch-1k-env-700e426c-postgres-2.c.gyoachum-d1847316.internal) and following a leader (refarch-1k-env-700e426c-postgres-1.c.gyoachum-d1847316.internal)
2022-07-06_07:41:36.70605 2022-07-06 07:41:36,705 INFO: no action. I am a secondary (refarch-1k-env-700e426c-postgres-2.c.gyoachum-d1847316.internal) and following a leader (refarch-1k-env-700e426c-postgres-1.c.gyoachum-d1847316.internal)
```



#### PostgreSQL


```
postgres-1:~# gitlab-ctl get-postgresql-primary
10.142.15.224:5432
```



#### Redis


```
gitlab-redis-cli --stat
------- data ------ --------------------- load -------------------- - child -
keys       mem      clients blocked requests            connections
380        4.45M    8       0       4579698 (+0)        10154
380        4.45M    8       0       4579717 (+19)       10154
380        4.45M    8       0       4579761 (+44)       10154
380        4.49M    8       0       4579793 (+32)       10154
```



#### Consul


```
consul-2:~# sudo /opt/gitlab/embedded/bin/consul members
Node                                                                      Address             Status  Type    Build  Protocol  DC             Segment
refarch-1k-env-700e426c-consul-1.c.gyoachum-d1847316.internal             10.142.15.199:8301  alive   server  1.9.6  2         gitlab_consul  <all>
refarch-1k-env-700e426c-consul-2.c.gyoachum-d1847316.internal             10.142.15.201:8301  alive   server  1.9.6  2         gitlab_consul  <all>
refarch-1k-env-700e426c-consul-3.c.gyoachum-d1847316.internal             10.142.15.203:8301  alive   server  1.9.6  2         gitlab_consul  <all>
refarch-1k-env-700e426c-gitaly-1.c.gyoachum-d1847316.internal             10.142.15.197:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-gitaly-2.c.gyoachum-d1847316.internal             10.142.15.200:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-gitaly-3.c.gyoachum-d1847316.internal             10.142.15.198:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-gitlab-rails-1.c.gyoachum-d1847316.internal       10.142.15.220:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-gitlab-rails-2.c.gyoachum-d1847316.internal       10.142.15.221:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-gitlab-rails-3.c.gyoachum-d1847316.internal       10.142.15.218:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-monitor-1.c.gyoachum-d1847316.internal            10.142.15.206:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-pgbouncer-1.c.gyoachum-d1847316.internal          10.142.15.217:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-pgbouncer-2.c.gyoachum-d1847316.internal          10.142.15.215:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-pgbouncer-3.c.gyoachum-d1847316.internal          10.142.15.212:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-postgres-1.c.gyoachum-d1847316.internal           10.142.15.224:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-postgres-2.c.gyoachum-d1847316.internal           10.142.15.223:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-postgres-3.c.gyoachum-d1847316.internal           10.142.15.222:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-praefect-1.c.gyoachum-d1847316.internal           10.142.15.194:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-praefect-2.c.gyoachum-d1847316.internal           10.142.15.196:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-praefect-3.c.gyoachum-d1847316.internal           10.142.15.195:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-praefect-postgres-1.c.gyoachum-d1847316.internal  10.142.15.204:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-redis-1.c.gyoachum-d1847316.internal              10.142.15.210:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-redis-2.c.gyoachum-d1847316.internal              10.142.15.207:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-redis-3.c.gyoachum-d1847316.internal              10.142.15.209:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-sidekiq-1.c.gyoachum-d1847316.internal            10.142.15.214:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-sidekiq-2.c.gyoachum-d1847316.internal            10.142.15.213:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-sidekiq-3.c.gyoachum-d1847316.internal            10.142.15.211:8301  alive   client  1.9.6  2         gitlab_consul  <default>
refarch-1k-env-700e426c-sidekiq-4.c.gyoachum-d1847316.internal            10.142.15.216:8301  alive   client  1.9.6  2         gitlab_consul  <default>
```

### [Troubleshooting Documentation ](https://docs.gitlab.com/ee/administration/postgresql/replication_and_failover.html#troubleshooting)
